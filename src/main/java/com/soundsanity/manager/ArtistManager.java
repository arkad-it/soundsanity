package com.soundsanity.manager;

import com.soundsanity.persistance.entity.Artist;
import com.soundsanity.persistance.repo.ArtistRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Optional;

@Service
public class ArtistManager {

    private ArtistRepo artistRepo;

    @Autowired
    public ArtistManager(ArtistRepo artistRepo) {
        this.artistRepo = artistRepo;
    }

    //FIND BY ID
    public Optional<Artist> findById(Long id) {
        return artistRepo.findById(id);
    }

    //FIND ALL
    public Iterable<Artist> findAll() {
        return artistRepo.findAll();
    }

    //SAVE & PUT & PATCH
    public Artist save(Artist newArtist) {
        return artistRepo.save(newArtist);
    }

    //DELETE
    public void deleteById(Long id) {
        artistRepo.deleteById(id);
    }

    //FIND ALL by USER ID
    public ArrayList<Artist> findAllByUserId(Long userId) {
        Iterable<Artist> all = artistRepo.findAll();
        ArrayList<Artist> userArtists = new ArrayList<>();
        for (Artist a:all
             ) {
            if(a.getUser().getUserId()==userId){
                userArtists.add(a);
            }
        }
        return userArtists;
    }


}
