package com.soundsanity.manager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.nio.charset.StandardCharsets;

@Service
public class TokenManager {

    private  HttpURLConnection con;

    @Autowired
    public String getToken()  {
        // zapis tokenu do stringbuildera
        StringBuilder tokenBuilder = new StringBuilder();
        // adres zapytania
        String baseUrl = "https://accounts.spotify.com/api/token";

        try {
            // zamiana adresu ze string na url
            URL url = new URL(baseUrl);
            // otwarcie połączenia z adresu url
            con = (HttpURLConnection) url.openConnection();
            // dodanie potrzebnych cech
            con.setDoOutput(true);
            con.setRequestMethod("POST");
            // nadanie własciwości
            con.setRequestProperty("User-Agent", "Java client");
            con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            // autoryzacja, basic token powstały z zaszyfrowanego polaczenia Client ID i Secret Client ID aplikacji, wymagane przez spotify
            con.setRequestProperty("Authorization", "Basic ODA3MGMwODdmZDlhNGE3YzgyZTAwNTViYjYxMjQwMmE6MDQ0ZmQzYjAwYzhlNDVmMGE1NmU4MTNmOWRmZWVmMTI=");
            // parametry body zapytania i ich zapis do output
            String urlParameters = "grant_type=client_credentials";
            byte[] postData = urlParameters.getBytes(StandardCharsets.UTF_8);
            try (DataOutputStream wr = new DataOutputStream(con.getOutputStream())) {
                wr.write(postData);
            }
            // zapis odpowiedzi od spotify
            StringBuilder content;
            try (BufferedReader br = new BufferedReader(
                    new InputStreamReader(con.getInputStream()))) {
                String line;
                content = new StringBuilder();
                while ((line = br.readLine()) != null) {
                    content.append(line);
                    content.append(System.lineSeparator());
                }
            }
            // wyciagniecie z json tylko tokena
            System.out.println("Entire response");
            System.out.println(content.toString());

            String[] split = content.toString().split(",");
            String[] tokenApos = split[0].split(":");
            String[] token = tokenApos[1].split("\"");
            tokenBuilder.append(token[1]);

            System.out.println("Token only");
            System.out.println(tokenBuilder.toString());

        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            con.disconnect();
        }
        String token = tokenBuilder.toString();
        return token;
    }
}
