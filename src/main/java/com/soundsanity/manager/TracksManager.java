package com.soundsanity.manager;

import com.soundsanity.persistance.entity.Tracks;
import com.soundsanity.persistance.repo.TracksRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Optional;

@Service
public class TracksManager {

    private TracksRepo tracksRepo;

    @Autowired
    public TracksManager(TracksRepo tracksRepo) {
        this.tracksRepo = tracksRepo;
    }

    //FIND BY ID
    public Optional<Tracks> findById(Long id) {
        return tracksRepo.findById(id);
    }

    //FIND ALL
    public Iterable<Tracks> findAll() {
        return tracksRepo.findAll();
    }

    //SAVE & PUT & PATCH
    public Tracks save(Tracks newTracks) {
        return tracksRepo.save(newTracks);
    }

    //DELETE
    public void deleteById(Long id) {
        tracksRepo.deleteById(id);
    }

    //FIND ALL by USER ID
    public ArrayList<Tracks> findAllByUserId(Long userId) {
        Iterable<Tracks> all = tracksRepo.findAll();
        ArrayList<Tracks> userTrackss = new ArrayList<>();
        for (Tracks a:all
             ) {
            if(a.getUser().getUserId()==userId){
                userTrackss.add(a);
            }
        }
        return userTrackss;
    }


}
