package com.soundsanity.manager;

import com.soundsanity.persistance.entity.Album;
import com.soundsanity.persistance.repo.AlbumRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Optional;

@Service
public class AlbumManager {

    private AlbumRepo albumRepo;

    @Autowired
    public AlbumManager(AlbumRepo albumRepo) {
        this.albumRepo = albumRepo;
    }

    //FIND BY ID
    public Optional<Album> findById(Long id) {
        return albumRepo.findById(id);
    }

    //FIND ALL
    public Iterable<Album> findAll() {
        return albumRepo.findAll();
    }

    //SAVE & PUT & PATCH
    public Album save(Album newAlbum) {
        return albumRepo.save(newAlbum);
    }

    //DELETE
    public void deleteById(Long id) {
        albumRepo.deleteById(id);
    }

    //FIND ALL by USER ID
    public ArrayList<Album> findAllByUserId(Long userId) {
        Iterable<Album> all = albumRepo.findAll();
        ArrayList<Album> userAlbums = new ArrayList<>();
        for (Album a:all
             ) {
            if(a.getUser().getUserId()==userId){
                userAlbums.add(a);
            }
        }
        return userAlbums;
    }


}
