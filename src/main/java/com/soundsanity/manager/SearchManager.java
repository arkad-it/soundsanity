package com.soundsanity.manager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Service;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

@Service
public class SearchManager {

    private static HttpURLConnection con;

    public String getArtistIdByNameSearch(String artist) {

        TokenManager tokenManager = new TokenManager();
        String token = null;
        token = tokenManager.getToken();
        System.out.println("This is our token" + token);
        // zapis tokenu do stringbuildera
        StringBuilder tokenBuilder = new StringBuilder();
        // adres zapytania
        String baseUrl = "https://api.spotify.com/v1/search?q="+ artist +"&type=artist&market=PL&limit=1";
        String outArtistId = "none";

        try {
            // String --> URL
            URL url = new URL(baseUrl);
            // Connection to URL
            con = (HttpURLConnection) url.openConnection();
            // Set
            con.setDoOutput(true);
            con.setRequestMethod("GET");
            // nadanie własciwości
            con.setRequestProperty("User-Agent", "Java client");
            // dodanie tokena do zapytania
            StringBuilder bearerToken = new StringBuilder();
            bearerToken.append("Bearer ").append(token);
            System.out.println(bearerToken.toString());
            con.setRequestProperty("Authorization", bearerToken.toString());

            // zapis odpowiedzi od spotify
            StringBuilder rawJson;
            try (BufferedReader br = new BufferedReader(
                    new InputStreamReader(con.getInputStream()))) {
                String line;
                rawJson = new StringBuilder();
                while ((line = br.readLine()) != null) {
                    rawJson.append(line);
                    rawJson.append(System.lineSeparator());
                }
            }
            // wyciagniecie z json tylko tokena
            System.out.println("Entire response: ");
            System.out.println(rawJson.toString());
            tokenBuilder.append(rawJson.toString());

        //-------------------------------------------------------------- RETRIEVE JSON ELEMENT

        // Create init JSON Obj.
        JSONObject jsonObject = new JSONObject(rawJson.toString());
        System.out.println("artist Object: " + jsonObject.toString());

        // Get nested JSON obj. from the entire 'jsonObject' result
        JSONObject artistObject = jsonObject.getJSONObject("artists");

        // Get 'artistObject's' result array
        // -> total search output list of positions;
        JSONArray itemsArray = artistObject.getJSONArray("items");
        System.out.println("Whole result: " + itemsArray.toString());

        // Retrieve every single artist from given list
        // -> in this case only 1. position is considered
        // -> get Artist ID;
        String artistId = "none";

        for (int i = 0; i < itemsArray.length(); i++){
            JSONObject foundArtist = itemsArray.getJSONObject(i);
            artistId = foundArtist.getString("id");
            System.out.println("Artist ID: " + artistId);
        }
        outArtistId = artistId;

        //-------------------------------------------------------------- RETRIEVE JSON ELEMENT

        } catch (JSONException | IOException e) {
            e.printStackTrace();
        } finally {
            con.disconnect();
        }

        return outArtistId;

    }

    public Map<String, String> getAlbumsByArtistId (String artistId) {

        TokenManager tokenManager = new TokenManager();
        String token = null;
        token = tokenManager.getToken();
        System.out.println("This is token: " + token);
        // zapis tokenu do stringbuildera
        StringBuilder tokenBuilder = new StringBuilder();
        // adres zapytania
        String baseUrl = "https://api.spotify.com/v1/artists/"+ artistId +"/albums";
        // Map(Key,Value)
        Map<String, String> albumMap = new LinkedHashMap<>(); //return value

        try {
            // zamiana adresu ze string na url
            URL url = new URL(baseUrl);
            // otwarcie połączenia z adresu url
            con = (HttpURLConnection) url.openConnection();
            // dodanie potrzebnych cech
            con.setDoOutput(true);
            con.setRequestMethod("GET");
            // nadanie własciwości
            con.setRequestProperty("User-Agent", "Java client");
            // dodanie tokena do zapytania
            StringBuilder bearerToken = new StringBuilder();
            bearerToken.append("Bearer ").append(token);
            System.out.println(bearerToken.toString());
            con.setRequestProperty("Authorization", bearerToken.toString());

            // zapis odpowiedzi od spotify
            StringBuilder rawJson;
            try (BufferedReader br = new BufferedReader(
                    new InputStreamReader(con.getInputStream()))) {
                String line;
                rawJson = new StringBuilder();
                while ((line = br.readLine()) != null) {
                    rawJson.append(line);
                    rawJson.append(System.lineSeparator());
                }
            }
            // wyciagniecie z json tylko tokena
            System.out.println("Entire response: ");
            System.out.println(rawJson.toString());
            tokenBuilder.append(rawJson.toString());

            //-------------------------------------------------------------- RETRIEVE JSON ELEMENT

            // Create init JSON Obj.
            JSONObject jsonObject = new JSONObject(rawJson.toString());
            System.out.println("Albums Object: " + jsonObject.toString());

            // Get result array
            // -> total search output list of positions;
            JSONArray itemsArray = jsonObject.getJSONArray("items");
            System.out.println("Whole result: " + itemsArray.toString());

            // Retrieve every single album from given list
            // -> create maps of key: 'albumId' and value: 'albumName';
            String albumId = "none";
            String albumName = "none";


            for (int i = 0; i < itemsArray.length(); i++){
                JSONObject foundAlbum = itemsArray.getJSONObject(i);
                albumId = foundAlbum.getString("id");
                albumName = foundAlbum.getString("name");
                albumMap.put(albumId, albumName);
            }


            Set<String> keys = albumMap.keySet();

            // display all albums
            for (String key : keys){
                System.out.println(key + " " + albumMap.get(key));
            }
            // display all albums using .toString()
            System.out.println("Hash.toString() :" + albumMap.toString());

            //-------------------------------------------------------------- RETRIEVE JSON ELEMENT

        } catch (JSONException | IOException e) {
            e.printStackTrace();
        } finally {
            con.disconnect();
        }

        return albumMap;

    }

    public Map<String, String> getAlbumTracksByAlbumId (String albumId) {

        TokenManager tokenManager = new TokenManager();
        String token = null;
        token = tokenManager.getToken();
        System.out.println("This is token: " + token);
        // zapis tokenu do stringbuildera
        StringBuilder tokenBuilder = new StringBuilder();
        // adres zapytania
        String baseUrl = "https://api.spotify.com/v1/albums/"+ albumId +"/tracks";
        // Map(Key,Value)
        Map<String, String> tracksMap = new LinkedHashMap<>(); //return value

        try {
            // zamiana adresu ze string na url
            URL url = new URL(baseUrl);
            // otwarcie połączenia z adresu url
            con = (HttpURLConnection) url.openConnection();
            // dodanie potrzebnych cech
            con.setDoOutput(true);
            con.setRequestMethod("GET");
            // nadanie własciwości
            con.setRequestProperty("User-Agent", "Java client");
            // dodanie tokena do zapytania
            StringBuilder bearerToken = new StringBuilder();
            bearerToken.append("Bearer ").append(token);
            System.out.println(bearerToken.toString());
            con.setRequestProperty("Authorization", bearerToken.toString());

            // zapis odpowiedzi od spotify
            StringBuilder rawJson;
            try (BufferedReader br = new BufferedReader(
                    new InputStreamReader(con.getInputStream()))) {
                String line;
                rawJson = new StringBuilder();
                while ((line = br.readLine()) != null) {
                    rawJson.append(line);
                    rawJson.append(System.lineSeparator());
                }
            }
            // wyciagniecie z json tylko tokena
            System.out.println("Entire response: ");
            System.out.println(rawJson.toString());
            tokenBuilder.append(rawJson.toString());

            //-------------------------------------------------------------- RETRIEVE JSON ELEMENT

            // Create init JSON Obj.
            JSONObject jsonObject = new JSONObject(rawJson.toString());
            System.out.println("Tracks Object: " + jsonObject.toString());

            // Get result array
            // -> total search output list of positions;
            JSONArray itemsArray = jsonObject.getJSONArray("items");
            System.out.println("Whole result: " + itemsArray.toString());

            // Retrieve every single track from given album
            // -> create maps of key: 'albumId' and value: 'albumName';
            String trackId = "none";
            String albumName = "none";


            for (int i = 0; i < itemsArray.length(); i++){
                JSONObject foundAlbum = itemsArray.getJSONObject(i);
                trackId = foundAlbum.getString("id");
                albumName = foundAlbum.getString("name");
                System.out.println(albumName);
                tracksMap.put(trackId, albumName);
            }


            Set<String> keys = tracksMap.keySet();

            // display all albums
            for (String key : keys){
                System.out.println(key + " " + tracksMap.get(key));
            }
            // display all albums using .toString()
            System.out.println("Hash.toString() :" + tracksMap.toString());

            //-------------------------------------------------------------- RETRIEVE JSON ELEMENT

        } catch (JSONException | IOException e) {
            e.printStackTrace();
        } finally {
            con.disconnect();
        }

        return tracksMap;

    }

    public Map<String, String> getArtistNameByArtistId (String artistId) {

        TokenManager tokenManager = new TokenManager();
        String token = null;
        token = tokenManager.getToken();
        System.out.println("This is token: " + token);
        // zapis tokenu do stringbuildera
        StringBuilder tokenBuilder = new StringBuilder();
        // adres zapytania
        String baseUrl = "https://api.spotify.com/v1/artists/"+ artistId;
        // Map(Key,Value)
        Map<String, String> artistMap = new HashMap<>(); //return value

        try {
            // zamiana adresu ze string na url
            URL url = new URL(baseUrl);
            // otwarcie połączenia z adresu url
            con = (HttpURLConnection) url.openConnection();
            // dodanie potrzebnych cech
            con.setDoOutput(true);
            con.setRequestMethod("GET");
            // nadanie własciwości
            con.setRequestProperty("User-Agent", "Java client");
            // dodanie tokena do zapytania
            StringBuilder bearerToken = new StringBuilder();
            bearerToken.append("Bearer ").append(token);
            System.out.println(bearerToken.toString());
            con.setRequestProperty("Authorization", bearerToken.toString());

            // zapis odpowiedzi od spotify
            StringBuilder rawJson;
            try (BufferedReader br = new BufferedReader(
                    new InputStreamReader(con.getInputStream()))) {
                String line;
                rawJson = new StringBuilder();
                while ((line = br.readLine()) != null) {
                    rawJson.append(line);
                    rawJson.append(System.lineSeparator());
                }
            }
            // wyciagniecie z json tylko tokena
            System.out.println("Entire response: ");
            System.out.println(rawJson.toString());
            tokenBuilder.append(rawJson.toString());

            //-------------------------------------------------------------- RETRIEVE JSON ELEMENT

            // Create init JSON Obj.
            JSONObject jsonObject = new JSONObject(rawJson.toString());
            System.out.println("Tracks Object: " + jsonObject.toString());

            // Get result array
            // -> total search output list of positions;

            String artistName = "none";

            artistName = jsonObject.getString("name");
            artistMap.put(artistId, artistName);

            Set<String> keys = artistMap.keySet();

            // display all albums
            for (String key : keys){
                System.out.println(key + " " + artistMap.get(key));
            }
            // display all albums using .toString()
            System.out.println("Hash.toString() :" + artistMap.toString());

            //-------------------------------------------------------------- RETRIEVE JSON ELEMENT

        } catch (JSONException | IOException e) {
            e.printStackTrace();
        } finally {
            con.disconnect();
        }

        return artistMap;

    }

}