package com.soundsanity.manager;

import com.soundsanity.persistance.entity.User;
import com.soundsanity.persistance.repo.UserRepo;
import com.soundsanity.security.PasswordEncoderConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.thymeleaf.model.IModel;

import java.util.Optional;

@Service
public class UserManager {

    private UserRepo userRepo;

    @Autowired
    public UserManager(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    //FIND BY ID
    public Optional<User> findById(Long id) {
        return userRepo.findById(id);
    }

    //FIND BY LOGIN
    public Optional<User> findByName(String userName) {

        Iterable<User> userIterable = findAll(); // getAll
        Optional<User> user = Optional.empty(); // empty user
        for (User u : userIterable
        ) {
            if (u.getUserName().equals(userName)) // if user in DB has the same name as given, then return this user
                user = Optional.ofNullable(u);
        }
        return user;
    }

    //FIND ALL
    public Iterable<User> findAll() {
        return userRepo.findAll();
    }

    //SAVE & PUT & PATCH
    public User save(User newUser) {
        return userRepo.save(newUser);
    }

    //DELETE
    public void deleteById(Long id) {
        userRepo.deleteById(id);
    }

    //REGISTER USER
    // only name and password
    public String registerUser(String userName, String password,String email) {
        System.out.println("Start register User");
        Optional<User> userExist = findByName(userName);
        if (userExist.isPresent()) {
            return "User exist";
        } else{
            User user = new User();
        // set name
        user.setUserName(userName);
        user.setEmail(email);
        // hash password and set
        PasswordEncoderConfig passwordEncoderConfig = new PasswordEncoderConfig();
        String passwordHash = passwordEncoderConfig.passwordEncoder().encode(password);
        user.setPassword(passwordHash);
        userRepo.save(user);
        return "User register successfuly";
    }

}

    //LOGIN
    public String login(String userName, String password) {
        Optional<User> user = findByName(userName);
        if (user.isPresent()) {
            PasswordEncoderConfig passwordEncoderConfig = new PasswordEncoderConfig();
            if (passwordEncoderConfig.passwordEncoder().matches(password, user.get().getPassword())) {
                return "Login successful";
            } else
                return "Bad password";
        } else
            return "User doesn't exist!";
    }
}