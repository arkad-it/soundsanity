package com.soundsanity.api.controller;

import com.soundsanity.manager.TracksManager;
import com.soundsanity.persistance.entity.Tracks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Optional;

@RestController
@RequestMapping("api")
public class TracksController {


    private TracksManager tracksManager;

    @Autowired
    public TracksController(TracksManager tracksManager) {
        this.tracksManager = tracksManager;
    }

    //GET BY ID
    @GetMapping("/tracks")
    public Optional<Tracks> getById(@RequestParam Long id) {
        return tracksManager.findById(id);
    }

    //GET ALL
    @GetMapping("/tracks/all")
    public Iterable<Tracks> getAll() {
        return tracksManager.findAll();
    }

    //POST
    @PostMapping("/tracks")
    public Tracks addTracks(@RequestBody Tracks newTracks) {
        return tracksManager.save(newTracks);
    }

    //PUT
    @PutMapping("/admin/tracks")
    public Tracks updateTracks(@RequestBody Tracks updatedTracks) {
        return tracksManager.save(updatedTracks);
    }
    //GET BY USER
    @GetMapping("/tracksByUserId")
    public ArrayList<Tracks> getAllByUserId(@RequestParam Long userId){
        return tracksManager.findAllByUserId(userId);
    }
    
    //PATCH
//    @RequestMapping(value = "/admin/tracks/{id}", method = RequestMethod.PATCH, consumes = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity<?> editTracks(@RequestBody Map<String, Object> updates, @PathVariable("id") Long id) {
//
//        Tracks editedTracks = tracksManager.findById(id).get();
//        Set<String> keySet = updates.keySet();
//
//        if (keySet.contains("info")) {
//            editedTracks.setInfo((String) updates.get("info"));
//        }
//
//        tracksManager.save(editedTracks);
//
//        return ResponseEntity.ok("Tracks edited successfully!");
//    }

    //DELETE
    @DeleteMapping("/tracks/user")
    public ResponseEntity<?> deleteTracks(@RequestParam Long id) {
        tracksManager.deleteById(id);
        return ResponseEntity.ok("Tracks deleted successfully!");
    }

}