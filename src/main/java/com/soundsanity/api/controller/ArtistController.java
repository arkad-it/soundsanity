package com.soundsanity.api.controller;

import com.soundsanity.manager.ArtistManager;
import com.soundsanity.persistance.entity.Artist;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

@RestController
@RequestMapping("api")
public class ArtistController {


    private ArtistManager artistManager;

    @Autowired
    public ArtistController(ArtistManager artistManager) {
        this.artistManager = artistManager;
    }

    //GET BY ID
    @GetMapping("/artist")
    public Optional<Artist> getById(@RequestParam Long id) {
        return artistManager.findById(id);
    }

    //GET ALL
    @GetMapping("/artist/all")
    public Iterable<Artist> getAll() {
        return artistManager.findAll();
    }

    //POST
    @PostMapping("/artist")
    public Artist addArtist(@RequestBody Artist newArtist) {
        return artistManager.save(newArtist);
    }

    //PUT
    @PutMapping("/admin/artist")
    public Artist updateArtist(@RequestBody Artist updatedArtist) {
        return artistManager.save(updatedArtist);
    }
    //GET BY USER
    @GetMapping("/artistByUserId")
    public ArrayList<Artist> getAllByUserId(@RequestParam Long userId){
        return artistManager.findAllByUserId(userId);
    }

    //PATCH
//    @RequestMapping(value = "/admin/artist/{id}", method = RequestMethod.PATCH, consumes = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity<?> editArtist(@RequestBody Map<String, Object> updates, @PathVariable("id") Long id) {
//
//        Artist editedArtist = artistManager.findById(id).get();
//        Set<String> keySet = updates.keySet();
//
//        if (keySet.contains("info")) {
//            editedArtist.setInfo((String) updates.get("info"));
//        }
//
//        artistManager.save(editedArtist);
//
//        return ResponseEntity.ok("Artist edited successfully!");
//    }

    //DELETE
    @DeleteMapping("/artist/user")
    public ResponseEntity<?> deleteArtist(@RequestParam Long id) {
        artistManager.deleteById(id);
        return ResponseEntity.ok("Artist deleted successfully!");
    }

}