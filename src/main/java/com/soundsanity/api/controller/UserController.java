package com.soundsanity.api.controller;

import com.soundsanity.manager.*;
import com.soundsanity.persistance.entity.Album;
import com.soundsanity.persistance.entity.Tracks;
import com.soundsanity.persistance.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import com.soundsanity.persistance.entity.Artist;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

@RestController
@RequestMapping("api")
public class UserController {

    private ArtistManager artistManager;
    private SearchManager searchManager;
    private UserManager userManager;
    private AlbumManager albumManager;
    private TracksManager tracksManager;


    @Autowired
    public UserController(ArtistManager artistManager, SearchManager searchManager, UserManager userManager, AlbumManager albumManager, TracksManager tracksManager) {
        this.artistManager = artistManager;
        this.searchManager = searchManager;
        this.userManager = userManager;
        this.albumManager = albumManager;
        this.tracksManager = tracksManager;
    }

    //GET BY ID
    @GetMapping("/user")
    public Optional<User> getById(@RequestParam Long id){
            return userManager.findById(id);
        }

    // REGISTER
    @PostMapping("/admin/user/register")
    public String register(@RequestParam String nameUser,@RequestParam String password,@RequestParam String email){
        return userManager.registerUser(nameUser,password,email);
    }
    //LOGIN
    @PostMapping("/user/login")
    public String login (@RequestParam String nameUser,@RequestParam String password){
        return userManager.login(nameUser, password);
    }
    //GET ALL
    @GetMapping("/user/all")
    public Iterable<User> getAll(){
            return userManager.findAll();
        }

    //POST
    @PostMapping("/user")
    public User addUser (@RequestBody User newUser){
            return userManager.save(newUser);
        }

    //PUT
    @PutMapping("/admin/user")
    public User updateUser (@RequestBody User updatedUser){
            return userManager.save(updatedUser);
        }

    //PATCH
    @RequestMapping(value = "/admin/user/{id}", method = RequestMethod.PATCH, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> editUser(@RequestBody Map<String, Object> updates, @PathVariable("id") Long id) {

        User editedUser = userManager.findById(id).get();
        Set<String> keySet = updates.keySet();

            if (keySet.contains("userName")){
                editedUser.setUserName((String) updates.get("userName"));
            }
            if (keySet.contains("status")){
            editedUser.setEmail((String)updates.get("email"));
            }
            if (keySet.contains("isEnabled")){
                editedUser.setEnabled((boolean)updates.get("isEnabled"));
            }
            if (keySet.contains("artistHashSet")){
                editedUser.setArtistSet((List<?>) updates.get("artistHashSet"));
            }


        userManager.save(editedUser);

        return ResponseEntity.ok("User edited successfully!");
    }

    //DELETE
    @DeleteMapping("/admin/user")
    public ResponseEntity<?> deleteUser (@RequestParam Long id){
    userManager.deleteById(id);
    return ResponseEntity.ok("User deleted successfully!");
    }

    //ADD FAVOURITE ARTIST
    @PostMapping("/user/add-favourite-band")
    public ResponseEntity<?> addFavouriteBand (@RequestParam String userName, @RequestParam String artistName){

        String FavArtistId = "";
        FavArtistId = searchManager.getArtistIdByNameSearch(artistName); // artistId
        Optional<User> user = userManager.findByName(userName);
        System.out.println("Optional user: " + user);

        String artistNameByArtistId = "none";
        if (!FavArtistId.isEmpty()) {
            artistNameByArtistId = String.valueOf(searchManager.getArtistNameByArtistId(FavArtistId).get(FavArtistId)); //artistName
        }

        if (!FavArtistId.isEmpty() && !artistNameByArtistId.isEmpty() && !artistNameByArtistId.isBlank()){
            User update = new User();
            update = user.get();
            List setUpdated = update.getArtistSet();
            setUpdated.add(artistNameByArtistId);
            update.setArtistSet(setUpdated);
            userManager.save(update);
        }

//        userManager.save();
        return ResponseEntity.ok("Artist added successfully!");
    }
    //ADD FAVOURITE ARTIST 2
    @PostMapping("/user/addArtist")
    public String addArtist(@RequestParam String userName,@RequestParam String name,@RequestParam String id){
        User user = userManager.findByName(userName).get();
        Artist artist = new Artist(id,name,user);
        artistManager.save(artist);
        return "Artist added to favourites";
    }
    //ADD FAVOURITE ALBUM
    @PostMapping("/user/addAlbum")
    public String addAlbum(@RequestParam String userName,@RequestParam String name,@RequestParam String id){
        User user = userManager.findByName(userName).get();
        Album album = new Album(id,name,user);
        albumManager.save(album);
        return "Album added to favourites";
    }
    //ADD FAVOURITE ALBUM
    @PostMapping("/user/addTracks")
    public String addTracks(@RequestParam String userName,@RequestParam String name,@RequestParam String id){
        User user = userManager.findByName(userName).get();
        Tracks tracks = new Tracks(id,name,user);
        tracksManager.save(tracks);
        return "Track added to favourites";
    }
}
