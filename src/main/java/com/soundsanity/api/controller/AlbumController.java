package com.soundsanity.api.controller;

import com.soundsanity.manager.AlbumManager;
import com.soundsanity.persistance.entity.Album;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Optional;

@RestController
@RequestMapping("api")
public class AlbumController {


    private AlbumManager albumManager;

    @Autowired
    public AlbumController(AlbumManager albumManager) {
        this.albumManager = albumManager;
    }

    //GET BY ID
    @GetMapping("/album")
    public Optional<Album> getById(@RequestParam Long id) {
        return albumManager.findById(id);
    }

    //GET ALL
    @GetMapping("/album/all")
    public Iterable<Album> getAll() {
        return albumManager.findAll();
    }

    //POST
    @PostMapping("/album")
    public Album addAlbum(@RequestBody Album newAlbum) {
        return albumManager.save(newAlbum);
    }

    //PUT
    @PutMapping("/admin/album")
    public Album updateAlbum(@RequestBody Album updatedAlbum) {
        return albumManager.save(updatedAlbum);
    }
    //GET BY USER
    @GetMapping("/albumByUserId")
    public ArrayList<Album> getAllByUserId(@RequestParam Long userId){
        return albumManager.findAllByUserId(userId);
    }
    
    //PATCH
//    @RequestMapping(value = "/admin/album/{id}", method = RequestMethod.PATCH, consumes = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity<?> editAlbum(@RequestBody Map<String, Object> updates, @PathVariable("id") Long id) {
//
//        Album editedAlbum = albumManager.findById(id).get();
//        Set<String> keySet = updates.keySet();
//
//        if (keySet.contains("info")) {
//            editedAlbum.setInfo((String) updates.get("info"));
//        }
//
//        albumManager.save(editedAlbum);
//
//        return ResponseEntity.ok("Album edited successfully!");
//    }

    //DELETE
    @DeleteMapping("/album/user")
    public ResponseEntity<?> deleteAlbum(@RequestParam Long id) {
        albumManager.deleteById(id);
        return ResponseEntity.ok("Album deleted successfully!");
    }

}