package com.soundsanity.api.controller;

import com.soundsanity.manager.SearchManager;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
public class SearchController {

    private SearchManager searchManager = new SearchManager();

    @GetMapping("search/artist")
    public String getArtistIdByNameSearch(@RequestParam String artistName)  {
        return searchManager.getArtistIdByNameSearch(artistName);
    }

    @GetMapping("search/artist-by-id")
    public String getArtistNameByArtistId(@RequestParam String artistId)  {
        Map map = new HashMap();
        map = searchManager.getArtistNameByArtistId(artistId);
        String artistName = (String) map.get(artistId);
        return artistName;
    }

    @GetMapping("search/artist/albums")
    public Map<String, String> getArtistAlbumsByArtistId(@RequestParam String artistId)  {
        return searchManager.getAlbumsByArtistId(artistId);
    }

    @GetMapping("search/album/tracks")
    public Map<String, String> getAlbumTracksByAlbumId(@RequestParam String albumId)  {
        return searchManager.getAlbumTracksByAlbumId(albumId);
    }

}
