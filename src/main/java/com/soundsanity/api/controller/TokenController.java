package com.soundsanity.api.controller;

import com.soundsanity.manager.TokenManager;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
public class TokenController {
    private TokenManager tokenManager = new TokenManager();


    @PostMapping("/getToken")
    public String getToken() {
        return tokenManager.getToken();
    }
}
