package com.soundsanity.persistance;

import com.soundsanity.persistance.entity.Artist;
import com.soundsanity.persistance.entity.User;
import com.soundsanity.persistance.repo.ArtistRepo;
import com.soundsanity.persistance.repo.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.jdbc.datasource.init.ScriptUtils;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.util.*;

@Component
public class DbMockData {

    private UserRepo userRepo;
    private ArtistRepo artistRepo;

    public DbMockData() {
    }

    @Autowired
    public DbMockData(UserRepo userRepo, ArtistRepo artistRepo) {
        this.userRepo = userRepo;
        this.artistRepo = artistRepo;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void fill() {

        String connectionUrl =
                "jdbc:h2:./SoundSanityDB;" + "user=sa;" + "password=;";


//        try (Connection connection = DriverManager.getConnection(connectionUrl);) {
//            executeSqlScript(connection, "drop all objects;");
//        }
//        // Handle any errors that may have occurred.
//        catch (SQLException e) {
//            e.printStackTrace();
//        }


    }

}