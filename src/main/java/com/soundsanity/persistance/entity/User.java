package com.soundsanity.persistance.entity;

import com.sun.istack.NotNull;

import javax.persistence.*;

import java.util.*;

@Entity
public class User {

    @NotNull
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="user_id")
    private long userId;
    @ElementCollection(targetClass=String.class)
    @Column(name="fav_artists")
    private List artistSet = new ArrayList<>();
    private String email;
    private String password;
    @Column(name="user_name")
    private String userName;
    private boolean isEnabled;


    public User() {
    }

    public User(String email, String password, String userName, boolean isEnabled, List<?> artistSet) {
        this.email = email;
        this.password = password;
        this.userName = userName;
        this.isEnabled = isEnabled;
        this.artistSet = artistSet;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public boolean isEnabled() {
        return isEnabled;
    }

    public void setEnabled(boolean enabled) {
        isEnabled = enabled;
    }

    public List<?> getArtistSet() {
        return artistSet;
    }

    public void setArtistSet(List<?> artistSet) {
        this.artistSet = artistSet;
    }

    @Override
    public String toString() {
        return "User{" +
                "userId=" + userId +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", userName='" + userName + '\'' +
                ", isEnabled=" + isEnabled +
                ", artistSet=" + artistSet +
                '}';
    }
}
