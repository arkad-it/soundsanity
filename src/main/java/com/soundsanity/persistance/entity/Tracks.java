package com.soundsanity.persistance.entity;

import com.sun.istack.NotNull;

import javax.persistence.*;

@Entity
public class Tracks {

    @NotNull
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String tracksId;
    private String tracksName;

    @ManyToOne
    private User user;

    public Tracks() {
    }

    public Tracks(Long id, String tracksId, String tracksName, User user) {
        this.id = id;
        this.tracksId = tracksId;
        this.tracksName = tracksName;
        this.user = user;
    }

    public Tracks(String tracksId, String tracksName, User user) {
        this.tracksId = tracksId;
        this.tracksName = tracksName;
        this.user = user;
    }

    public String getArtistId() {
        return tracksId;
    }

    public void setArtistId(String tracksId) {
        this.tracksId = tracksId;
    }

    public String getArtistName() {
        return tracksName;
    }

    public void setArtistName(String tracksName) {
        this.tracksName = tracksName;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
