package com.soundsanity.persistance.entity;

import com.sun.istack.NotNull;

import javax.persistence.*;

@Entity
public class Album {

    @NotNull
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String albumId;
    private String albumName;

    @ManyToOne
    private User user;

    public Album() {
    }

    public Album(Long id, String albumId, String albumName, User user) {
        this.id = id;
        this.albumId = albumId;
        this.albumName = albumName;
        this.user = user;
    }

    public Album(String albumId, String albumName, User user) {
        this.albumId = albumId;
        this.albumName = albumName;
        this.user = user;
    }

    public String getArtistId() {
        return albumId;
    }

    public void setArtistId(String albumId) {
        this.albumId = albumId;
    }

    public String getArtistName() {
        return albumName;
    }

    public void setArtistName(String albumName) {
        this.albumName = albumName;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
