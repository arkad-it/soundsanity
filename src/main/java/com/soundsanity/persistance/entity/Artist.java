package com.soundsanity.persistance.entity;

import com.sun.istack.NotNull;

import javax.persistence.*;

@Entity
public class Artist {

    @NotNull
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String artistId;
    private String artistName;

    @ManyToOne
    private User user;

    public Artist() {
    }

    public Artist(Long id, String artistId, String artistName, User user) {
        this.id = id;
        this.artistId = artistId;
        this.artistName = artistName;
        this.user = user;
    }

    public Artist(String artistId, String artistName, User user) {
        this.artistId = artistId;
        this.artistName = artistName;
        this.user = user;
    }

    public String getArtistId() {
        return artistId;
    }

    public void setArtistId(String artistId) {
        this.artistId = artistId;
    }

    public String getArtistName() {
        return artistName;
    }

    public void setArtistName(String artistName) {
        this.artistName = artistName;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
