package com.soundsanity.persistance.repo;

import com.soundsanity.persistance.entity.Album;
import com.soundsanity.persistance.entity.Tracks;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TracksRepo extends CrudRepository<Tracks, Long> {
}