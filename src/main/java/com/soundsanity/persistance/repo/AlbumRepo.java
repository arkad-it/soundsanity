package com.soundsanity.persistance.repo;

import com.soundsanity.persistance.entity.Album;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AlbumRepo extends CrudRepository<Album, Long> {
}